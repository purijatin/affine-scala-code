organization := "com.jatinpuri"

name := "scalahandson"

version := "1.0"

scalaVersion := "2.12.10"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.5" % "test"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

