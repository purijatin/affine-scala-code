package part2


object ListPrac2 {
  /**
    * Duplicate each element of the list
    *
    * For example:
    * ```
    * Example:
    * scala> duplicate(List('a, 'b, 'c, 'c, 'd))
    * res0: List[Symbol] = List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd)
    * ```
    *
    * @param ls list
    * @tparam T type of list
    * @return
    */
  def duplicate[T](ls: List[T]): List[T] = ???

  /**
    * Duplicate each element of the list
    *
    * For example:
    * ```
    * Example:
    * scala> duplicateN(List('a, 'b), 3)
    * res0: List[Symbol] = List('a, 'a, 'a, 'b, 'b, 'b)
    * ```
    *
    * @param ls list
    * @tparam T type of list
    * @return
    */
  def duplicateN[T](ls: List[T], n:Int): List[T] = ???
}
