package part2

import scala.util.Try


object OtherProblems {
  /**
    * Takes a string. Sample input: "1,2,3,4,5,6,7"
    * You need to find the sum of all numbers in the string.
    *
    * of-course you can have invalid arguments as well
    * @param n integers separated by comma
    * @return Success of sum of the integers. Else Failure with the cause
    */
  def getSqrt(n: => String): Try[Int] = ???

  /**
    * This method print's the value in case it is a success. Else it prints
    * `Oops!` in case of failure
    *
    * @param value
    */
  def print(value: Try[String]): Unit = ???


  /**
    * returns a `Some(ans)` in case the argument is a success. Else returns None
    * @param n Try which could either be a success or a failure
    * @tparam T type of the value it contains
    * @return Option value
    */
  def convertToOption[T](n: => Try[T]) = ???

  /**
    * Takes a string. Sample input: "1,2,3,4,5,6,7"
    * Finds the sum of integers.
    *
    * If any number is invalid, returns None
    *
    * @param n
    * @return
    */
  def getSqrtAsOption(n: => String): Option[Int] = ???
}
