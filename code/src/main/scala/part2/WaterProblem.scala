package part2

/*
Create a case class WaterBottle that has two arguments:
    brand:String and litre: Int
Create a list of WaterBottle that will contain 3 water bottle objects:
    WaterBottle("bisleri",1), WaterBottle("bisleri",2),
      WaterBottle("himalayan",1)
Implement using for print all the objects that has brand bisleri
 */

case class WaterBottle(brand: String, litre: Int)

object WaterProblem {
  def main(args: Array[String]): Unit = {

  }
}

class WaterStats(ls: List[WaterBottle]) {

  /**
    * Returns names of all brands from the list.
    * * For example
    * ```
    * //if ls = List(WaterBottle("bisleri",1), WaterBottle("himalayan",1))
    * getNamesOfAllBrands must return List("bisleri", "himalayan")
    * ```
    */
  def getNamesOfAllBrands: List[String] = ???

  /**
    * `getNamesOfAllBrands` can contain duplicate brands if there are multiple bottles of the same branc in `ls`.
    * This method must remove duplicates
    */
  def getUniqueBrands: Set[String] = ???

  /**
    * Implement using for-yield obtain a collection of all litres capacity used in list above.
    * For example
    * ```
    * if ls = List(WaterBottle("bisleri",1), WaterBottle("bisleri",2))
    * getAllLitreCapacities must return 3
    * ```
    */
  def getTotalWaterCapacity: Int = ???

  /**
    * Implement using for-yield obtain a collection
    * of all objects with litre capacity as twice the original one
    * ```
    * if ls = List(WaterBottle("bisleri",1))
    * doubleCapacity must return List(WaterBottle("bisleri",2))
    * ```
    */
  def doubleCapacity: List[WaterBottle] = ???

  /**
    * Gets all the bottles in `ls` whose name matches the argument
    *
    * @param name brand name. Example: Aqua
    */
  def getAllBottlesOfBrand(name: String): List[WaterBottle] = ???
}

