package part1


object ListPrac {
  /**
    * create a list containing numbers from 1 to 10
    **/
  def oneToTen: List[Int] = ???

  /**
    * Get the first element of the list. 
    *
    * @param ls the argument list size is always greater than 0
    */
  def getFirstElement(ls: List[Int]): Int = ???

  /**
    * Get the last element of the list
    *
    * @param ls the argument list size is always greater than 0
    */
  def getLastElement(ls: List[Int]): Int = ???

  /**
    * Get the size of the list
    */
  def size(ls: List[Int]): Int = ???

  /**
    * Get first `n` elements of the List. For example:
    * ```
    * getFirstSix(List(1,2,3), 2)
    * //must return List(1,2)
    * ```
    *
    * @param n assume n is always less than size of the list
    */
  def getFirstN(ls: List[Int], n: Int): List[Int] = ???

  /**
    * Get all elements of the list, excluding the last element
    *
    * @param ls argument list size is greater than or equal to 1
    */
  def excludeLastAndGet(ls: List[Int]): List[Int] = ???

  /**
    * Get last `n` elements of the List
    *
    * @param n n is greater than or equal to the size of the list
    */
  def getLastNElements(ls: List[Int], n: Int): List[Int] = ???

  /**
    * Get all the elements of the list after excluding the head element
    *
    * @param ls size of the list is greater than or equal to one
    */
  def excludeHeadAndGet(ls: List[Int]): List[Int] = ???

  /**
    * See if the list contains element: `num`
    */
  def containsNum(ls: List[Int], num: Int): Boolean = ???

  /**
    * Get the element at index `index`
    */
  def getAtIndex(ls: List[Int], index: Int): Int = ???

  /**
    * Add an element to the head of the List
    */
  def appendToHead(ls: List[Int], num: Int): List[Int] = ???

  /**
    * Append `ls1` and `ls2` with `ls1` in front.
    * For example: `appendList(List(1,2), List(3))` must return `List(1,2,3)`
    */
  def appendList(ls1: List[Int], ls2:List[Int]): List[Int] = ???

  /**
    * Gets the penultimate number of the list. i.e. last exception one element
    * @param ls list
    * @tparam T type of the list
    * @return penultimate
    */
  def penultimate[T](ls:List[T]):T = ???

  /**
    * Reverses the list
    * @param ls list
    * @tparam T type of list
    * @return reversed list
    */
  def reverse[T](ls:List[T]): List[T] = ???
}
