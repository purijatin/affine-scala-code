package part1

import org.scalatest.{Matchers, WordSpec}

/**
  * Created by jatin on 6/4/17.
  */
class FractionEqualitySpec extends WordSpec with Matchers {
  "equals" should {
    "must be true in case of same objects" in {
      new Fraction(1,2) shouldEqual new Fraction(1,2)
      val f = new Fraction(2,3)
      f shouldEqual f
    }

    "numerator must match" in {
      new Fraction(1,2) should not equal new Fraction(1,3)
    }

    "denominator must match" in {
      new Fraction(1,2) should not equal new Fraction(3,1)
    }
  }
}

