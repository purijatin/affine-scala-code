package part2

import java.io.{BufferedWriter, File, FileOutputStream, OutputStreamWriter}

import org.scalactic.TolerantNumerics
import org.scalatest.{Matchers, WordSpec}

import scala.io.Source

class MeanStdDevSpec extends WordSpec with Matchers {
  //  val b = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("/home/jatin/work/trainings/affine/affine-scala-code/code/src/main/resources/runtime.txt"))))
  //
  //  for{
  //    _ <- 1 to 1000
  //    n = Random.nextDouble() * 100
  //  } {
  //    b.write(n+"sec\n")
  //  }
  //  b.close()

  "Statistics" should {
    "calculate mean and std dev" in {
      val file = new File("temp.txt")
      file.deleteOnExit()
      val b = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))
      for {
        _ <- 1 to 1000
        n = 1
      } {
        b.write(n + "sec\n")
      }
      b.close()

      val s = new StatisticsCalculator(Source.fromFile(file))
      s.getMean shouldEqual 1
      s.variance shouldEqual 0
    }

    "calculate mean and std dev - better example" in {
      val file = new File("temp.txt")
      file.deleteOnExit()
      val b = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))
      for {
        n <- 1 to 10
      } {
        b.write(n + "sec\n")
      }
      b.close()

      implicit val doubleEquality = TolerantNumerics.tolerantDoubleEquality(0.001)

      val s = new StatisticsCalculator(Source.fromFile(file))
      assert(s.getMean === 5.5)
      assert(s.variance === 8.25)
      assert(s.stdDev === 2.87228)
    }
  }

}