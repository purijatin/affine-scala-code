package part2

import part1.ListPrac
import org.scalatest.{Matchers, WordSpec}

/**
  * Created by jatin on 7/4/17.
  */
class ListPrac2Spec extends WordSpec with Matchers {
  "ListPrac2" should {
    "operations defined well" in {
      ListPrac2.duplicate(List(1,2,3,4,5)) shouldEqual List(1,1,2,2,3,3,4,4,5,5)
      ListPrac2.duplicate(List()) shouldEqual List()
      ListPrac2.duplicate(List('a,'b,'c)) shouldEqual List('a,'a,'b,'b,'c,'c)

      ListPrac2.duplicateN(List('a),5) shouldEqual List('a,'a,'a,'a,'a)
      ListPrac2.duplicateN(List('b,'a),1) shouldEqual List('b,'a)
      ListPrac2.duplicateN(List('b,'a),3) shouldEqual List('b,'b,'b,'a,'a,'a)
    }
  }

}
