package part2

import org.scalatest.{Matchers, WordSpec}


class WaterSpec extends WordSpec with Matchers {
  "WaterBottle" should {
    "correct calculation" in {
      val bottles = List(WaterBottle("bisleri",1), WaterBottle("bisleri",2), WaterBottle("himalayan",1), WaterBottle("Aqua",3))
      val prob = new WaterStats(bottles)

      prob.getNamesOfAllBrands shouldEqual List("bisleri", "bisleri","himalayan", "Aqua")
      prob.getUniqueBrands shouldEqual Set("bisleri","himalayan", "Aqua")
      prob.getTotalWaterCapacity shouldEqual 7
      prob.doubleCapacity shouldEqual
        List(WaterBottle("bisleri",2), WaterBottle("bisleri",4), WaterBottle("himalayan",2), WaterBottle("Aqua",6))


      prob.getAllBottlesOfBrand("bisleri") shouldEqual List(WaterBottle("bisleri",1), WaterBottle("bisleri",2))
      prob.getAllBottlesOfBrand("blah blah") shouldEqual List()
    }
  }
}
